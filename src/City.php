<?php

class City
{
   const CITIES = ['Bordeaux', 'Paris'];
   const CITIES_POPULATION = ['Bordeaux' => 249712, 'Paris' => 2_161_000];

   public function __construct(
       private string $name = '',
       private bool $isCapital = true,
       private int $people = 0
   )
   {}

   public function getName():string {return $this->name;}
   public function getIsCapital():bool {return $this->isCapital;}
   public function getPeople():int {return $this->people;}

   public function getCityNameById($id):string
   {
       if ($id == 1) {
           $city = "Bordeaux";
       } else {
           $city = "Paris";
       }
       return $city;
   }

   public function getPopulationByName(string $name):int
   {
       foreach (self::CITIES as $city){
        if ($city === $name) return self::CITIES_POPULATION[$city];
       }
       return 0;
   }
}
