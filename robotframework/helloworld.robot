*** Settings ***
Resource         resources/common.resource
Suite Setup      Open browser to home page
Suite Teardown   Close browser

*** Test Cases ***
Valid Hello World text
    Go To    ${HELLO WORLD URL}
    Wait Until Page Contains    Hello