<?php

require_once __DIR__ . '/../src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../src/City.php");

class CityTest extends \PHPUnit\Framework\TestCase
{
   public function testgetCityNameById()
   {
       $city = new City();
       $result = $city->getCityNameById(1);
       $expected = 'Bordeaux';
       $this->assertTrue($result == $expected);
   }

   public function testIsCapital()
   {
       $city = new City();
       $result = $city->getIsCapital();
       $expected = true;
       $this->assertTrue($result === $expected);
   }

   public function testGetPopulationByName()
   {
       $city = new City();
       $result = $city->getPopulationByName('Bordeaux');
       $expected = City::CITIES_POPULATION['Bordeaux'];
       $this->assertTrue($result === $expected);
   }
}
